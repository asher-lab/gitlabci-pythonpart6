from flask import Flask
import os

app = Flask(__name__)


# Which url call the associated function.
@app.route("/")
def wish():
    message = "Happy Birthday {name}"
    return message.format(name=os.getenv("NAME", "John"))

if __name__ == "__main__":
  app.run(port=7777)
